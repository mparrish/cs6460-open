var express = require('express');
var app = express();
var path = require('path');
// var cookie = require('cookie');
var cookieParser = require('cookie-parser');
var mustacheExpress = require('mustache-express');
var bodyParser = require('body-parser');
var MongoClient = require('mongodb').MongoClient
var assert = require('assert');

// Register '.html' extension with The Mustache Express
app.engine('html', mustacheExpress());

app.set('view engine', 'mustache');
app.set('views', __dirname + '/public');


// app.use(express.static('public'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser())
// to support JSON-encoded bodies
app.use(bodyParser.json());
// to support URL-encoded bodies     
app.use(bodyParser.urlencoded({
  extended: true
})); 

var url = 'mongodb://localhost:27017/edutech';

app.get('/', function (req, res) {
  console.log('root called');
});

app.get('/content', function (req, res) {
  console.log('content called');
  res.render('content.html');
});

app.get('/login', function (req, res) {
  console.log('login called');
  res.render('login.html');
});

app.post('/loginuser', function (req, res) {
  console.log('loginuser post called');
  
  MongoClient.connect(url, function(err, db) {
    assert.equal(null, err);

    // todo: for dev purposes password isn't encrypted, but needs to be
    var user = db.users.find( { email: req.body.email, password: req.body.password } );

    if (user.length === 1) {
      res.render('index.html');
    } else {
      res.render('invalidlogin.html');
    }

    db.close();
  });
});

app.get('/signup', function (req, res) {
  console.log('signup called');
  res.render('signup.html');
});

app.post('/signupuser', function (req, res) {
  console.log('signupuser called');
  
  MongoClient.connect(url, function(err, db) {
    assert.equal(null, err);

    // todo: for dev purposes password isn't encrypted, but needs to be
    if ((req.body.inputPassword !== req.body.inputPassword2) && req.body.inputEmail.length < 5) {
      res.render('invalidsignup.html');
      return;
    }

    db.users.insert( {
      email: req.body.inputEmail,
      password: req.body.inputPassword,
      signupDate: new Date()
    });

    db.close();
    res.render('index.html');
  });
});

app.post('/savecontent', function(req, res) {
  console.log('savecontent called');
  var title = req.body.title;
  var content = req.body.content;
  console.log('title: ' + title);
  console.log('content: ' + content);
  if (title.length > 0 && content.length > 0) {
    MongoClient.connect(url, function(err, db) {
      assert.equal(null, err);

      db.content.insert( {
        title: title,
        content: content
      });

      db.close();
      res.send('content.html');
    });
  }
});

app.listen(3000, function () {
  console.log('App listening on port 3000!');
});